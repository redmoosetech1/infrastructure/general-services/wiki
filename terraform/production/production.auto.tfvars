machines = {
 wiki = {
  template = "rocky8.5_server_amd64_template"
  folder = "Infrastructure/Production/General"
  hostname = "wiki"
  domain = "rmt"
  network = "General"
  ipv4_address = "172.16.30.30"
  ipv4_netmask = 24
  ipv4_gateway = "172.16.30.254"
  dns_domain = "rmt"
  dns_servers = ["172.16.0.17", "172.16.0.18"]
  num_cpus = 4
  num_cores_per_socket = 4
  memory = 4096
  datastore_1 = "esx0_local_storage"
  datastore_2 = "Storage"
  disks = []
 }
}
